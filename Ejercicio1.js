//Ejemplo 1
// Uso de intervalos en codigo
///Areglo de frutas
const frutas = ['manzana','naranja','pera','toronja'];
console.log("Impresion de listado de frutas");
//Impresion de cada fruta
frutas.map((fruta)=>console.log(fruta));

//Filtro de numeros pares e impares
//Arreglo de  numeros
const numeros = [1,2,3,4,5,6,7,8,9,10];
//Numeros pares
const numerosPares=numeros.filter((numero)=>numero%2===0);
console.log('Busqueda de numeros pares');
console.log(numerosPares);
//Numeros impares
const numerosImpares=numeros.filter((numero)=>numero%2!==0);
console.log('Busqueda de numeros impares');
console.log(numerosImpares);
///Filtro de busqueda de fruta que contiene la letra e
console.log('Fruta que contiene la e');
frutas.forEach((fruta)=> {
    if(fruta.includes('e')){
        console.log(fruta);
    }
});
///Suma de numeros pares
let suma_de_pares=0;
numeros.forEach((numero)=>{
    if(numero%2===0){
        suma_de_pares +=numero;
    }
} );
console.log('Suma de numeros pares');
console.log(suma_de_pares + ' - Se sumó:' + numerosPares );
//Suma de numeros impares
let suma_de_impares=0;
numeros.forEach((numero)=>{
    if(numero%2!==0){
        suma_de_impares +=numero;
    }
} );
console.log('Suma de numeros impares');
console.log(suma_de_impares + ' - Se sumó:' + numerosImpares );